<?php

declare ( strict_types = 1 );

namespace Application\Components;

use Phalcon\Mvc\User\Plugin;
use Application\Components\Status as Status;
use Application\Components\restResponse as restResponse;
use Application\Exceptions\httpException as httpException;
use \Firebase\JWT\JWT;

class Auth extends Plugin
{

    const UNAUTHENTICATED_USER_ROLE = 'Guest';
    private $decodedToken = null;

    public function __construct ()
    {

        if (
            $this -> di -> getShared ( 'configuration' ) -> hasAccessibleResourceWithoutAuthentication &&
            !$this -> di -> getShared ( 'request' ) -> hasHeader ( 'Authorization' )
        )
        return;

        if ( !$this -> di -> getShared ( 'request' ) -> hasHeader ( 'Authorization' ) )
        {
            $restResponse = new restResponse ( new Status\httpStatus401(), new Status\restStatusError(), new restMessage ('Missing authorization header.' ) );
            throw new httpException ( 'Missing authorization header.', $restResponse, 1201 );
        }

        list ( $encodedToken ) = sscanf ( $this -> di -> getShared ( 'request' ) -> getHeader ( 'Authorization' ), 'Bearer %s' );
        if ( !$encodedToken )
        {
            $restResponse = new restResponse ( new Status\httpStatus401(), new Status\restStatusError(), new restMessage ( 'Missing authorization token.' ) );
            throw new httpException ('Missing authorization token.', $restResponse, 1202 );
        }

        try
        {
            $decodedToken = JWT::decode ( $encodedToken, $this -> di -> getShared ( 'configuration' ) -> jwtKey, array ( 'HS512' ) );
        }
        catch ( \Firebase\JWT\ExpiredException $e )
        {
            $restResponse = new restResponse ( new Status\httpStatus401(), new Status\restStatusError(), new restMessage ('Authorization token has expired.' ) );
            throw new httpException ('Authorization token has expired.', $restResponse, 1203 );
        }
        catch ( \Firebase\JWT\BeforeValidException $e )
        {
            $restResponse = new restResponse ( new Status\httpStatus401(), new Status\restStatusError(), new restMessage ('Authorization token is not yet valid.' ) );
            throw new httpException ( $e -> getMessage (), $restResponse, 1204 );
        }
        catch ( \UnexpectedValueException | \Firebase\JWT\SignatureInvalidException $e )
        {
            $restResponse = new restResponse ( new Status\httpStatus401(), new Status\restStatusError(), new restMessage ('Invalid authorization token.' ) );
            throw new httpException ('Invalid authorization token: ' . $e -> getMessage (), $restResponse, 1205 );
        }

        $this -> decodedToken = $decodedToken;

    }

    public function getRoles ()
    {
        if ( isset ( $this -> decodedToken -> role ) )
            return $this -> decodedToken -> role;
        return self::UNAUTHENTICATED_USER_ROLE;
    }

    public function getToken ()
    {
        return $this -> decodedToken;
    }

    public function __destruct ()
    {}

}