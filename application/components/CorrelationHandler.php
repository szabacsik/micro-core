<?php
declare ( strict_types = 1 );

namespace Application\Components;

use Phalcon\Mvc\User\Plugin;
use Phalcon\Security\Random;

class CorrelationHandler extends Plugin
{

    # https://stackoverflow.com/questions/19989481/how-to-determine-if-a-string-is-a-valid-v4-uuid
    const UUID4 = '/^[0-9A-F]{8}-[0-9A-F]{4}-4[0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i';
    const CORRELATION_HTTP_HEADER_FIELD_NAME = 'X-Correlation-Id';

    private $correlationId = null;

    public function __construct ()
    {
        if
        (
            $this -> requestHeaderFieldIsSet () &&
            $this -> requestHeaderValueFormatIsValid ()
        )
            $this -> setCorrelationId ( $this -> getRequestHeaderValue () );
        elseif ( $this -> di -> getShared ( 'configuration' ) -> provideCorrelationId )
            $this -> setCorrelationId ( $this -> provideUniqueValue () );
        $dump = $this -> di -> getShared ( 'dump' );
        $dump -> add ( [ 'correlationId' => $this -> correlationId ] );
    }

    public function getCorrelationId ()
    {
        return $this -> correlationId;
    }

    public function provideUniqueValue ()
    {
        $random = new Random ();
        return $random -> uuid ();
    }

    public function requestHeaderFieldIsSet ()
    {
        return isset ( $this -> di -> getShared ( 'request' ) -> getHeaders () [ self::CORRELATION_HTTP_HEADER_FIELD_NAME ] );
    }

    public function requestHeaderValueFormatIsValid ()
    {
        return $this -> validCorrelationIdFormat ( $this -> di -> getShared ( 'request' ) -> getHeader ( self::CORRELATION_HTTP_HEADER_FIELD_NAME ) );
    }

    public function validCorrelationIdFormat ( $value )
    {
        if ( !is_string ( $value ) )
            return false;
        return preg_match ( self::UUID4, $value );
    }

    public function getRequestHeaderValue ()
    {
        return $this -> di -> getShared ( 'request' ) -> getHeader ( self::CORRELATION_HTTP_HEADER_FIELD_NAME );
    }

    private function setCorrelationId ( $uniqueness )
    {
        $this -> correlationId = $uniqueness;
    }

}
