<?php
declare ( strict_types = 1 );
namespace Application\Components;
use Application\Components\Status\httpStatus;
use Application\Components\Status\restStatus;

class restResponse
{
    use payloadTrait;
    public $httpStatus = null;
    public $status = null;
    public $messages = null;
    public $errors = null;

    public function __construct ( httpStatus $httpStatus, restStatus $restStatus, restMessage $restMessage = null, $payload = null )
    {
        if ( $restMessage != null )
        {
            $restStatusClass = ( new \ReflectionClass ( $restStatus ) ) -> getShortName ();
            if ( $restStatusClass === 'restStatusError' )
                $this -> addError ( $restMessage );
            elseif ( $restStatusClass === 'restStatusOk' )
                $this -> addMessage ( $restMessage );
            else
                throw new \InvalidArgumentException ();
        }
        $this -> setHttpStatus ( $httpStatus );
        $this -> setStatus ( $restStatus );
        $this -> addPayload ( $payload );
        if ( is_null ( $this -> messages ) )
            unset ( $this -> messages );
        if ( is_null ( $this -> errors ) )
            unset ( $this -> errors );
        if ( is_null ( $this -> payload ) )
            unset ( $this -> payload );
    }

    public function setHttpStatus ( httpStatus $httpStatus )
    {
        $this -> httpStatus = $httpStatus;
    }

    public function setStatus ( restStatus $restStatus )
    {
        $this -> status = $restStatus -> code;
    }

    public function addMessage ( restMessage $restMessage )
    {
        $this -> messages [] = $restMessage;
    }

    public function addError ( restMessage $restMessage )
    {
        $this -> errors [] = $restMessage;
    }
}
