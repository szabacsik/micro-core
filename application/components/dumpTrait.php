<?php
declare ( strict_types = 1 );
namespace Application\Components;

trait dumpTrait
{
    private $dump = null;
    final public function addDump ( $dump )
    {
        if ( is_null ( $dump ) )
            return;
        if ( is_null ( $this -> dump ) )
            $this -> dump = [];
        $this -> dump [] = $dump;
    }
    final public function getDump ()
    {
        if ( isset ( $this -> dump ) )
            return $this -> dump;
        else
            return null;
    }
}
