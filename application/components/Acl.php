<?php

declare ( strict_types = 1 );

namespace Application\Components;

use Application\Components\Status as Status;
use Application\Components\restResponse as restResponse;
use Application\Exceptions\httpException as httpException;
use Phalcon\Mvc\User\Plugin;
use Phalcon\Acl\Adapter\Memory as AclMemory;
use Phalcon\Acl\Role;
use Phalcon\Acl\Resource;

class Acl extends Plugin
{
    private $acl = null;

    public function __construct ()
    {
    }

    public function getAcl ()
    {
        if ( is_object ( $this -> acl ) )
            return $this -> acl;
        $this -> acl = new AclMemory ();
        $this -> acl -> setDefaultAction ( \Phalcon\Acl::DENY );
        $this -> acl -> setNoArgumentsDefaultAction ( \Phalcon\Acl::DENY );

        $roleGuest = new Role ( 'Guest', 'Visitors that have not logged in' );
        $this -> acl -> addRole ( $roleGuest );

        $roleUser = new Role ( 'User', 'Authenticated principal that have logged in' );
        $this -> acl -> addRole ( $roleUser );

        $roleAdministrator = new Role ( 'Administrator', 'Site managers who has the most comprehensive level of permissions' );
        $this -> acl -> addRole ( $roleAdministrator );

        $this -> acl -> addResource ( new Resource ( 'Application\Controllers\TestController' ), [ 'index', 'sensitive', 'confidential', 'forward', 'upstream' ] );

        $this -> acl -> allow ( $this -> di -> getShared ( 'auth' )::UNAUTHENTICATED_USER_ROLE, 'Application\Controllers\TestController', 'index' );
        $this -> acl -> allow ( 'User', 'Application\Controllers\TestController', [ 'sensitive', 'forward', 'upstream' ] );
        $this -> acl -> allow ( 'Administrator', 'Application\Controllers\TestController', 'confidential' );
        $this -> acl -> addInherit ( $roleUser, $roleGuest );
        $this -> acl -> addInherit ( $roleAdministrator, $roleUser );

        return $this -> acl;
    }

    public function handle ( array $activeHandler )
    {
        $acl = $this -> getAcl ();
        $roles = [];
        $role = $this -> di -> getShared ( 'auth' ) -> getRoles ();
        if ( !is_array ( $role ) )
            $roles [] = $role;
        else
            $roles = $role;
        if ( !count ( $roles ) )
            throw new \InvalidArgumentException ( "Empty user role." );
        $allowed = false;
        foreach ( $roles as $role )
        {
            $allowed = $acl -> isAllowed ( $role, $this -> getControllerName ( $activeHandler ), $this -> getActionName ( $activeHandler ) );
            if ( $allowed )
                break;
        }
        if ( !$allowed )
        {
            $restResponse = new restResponse ( new Status\httpStatus403(), new Status\restStatusError(), new restMessage ('Accessing the resource is forbidden, caused by incorrect ownership or permissions.' ) );
            throw new httpException ( 'Accessing the resource is forbidden, caused by incorrect ownership or permissions.', $restResponse, 1357 );
        }
        return true;
    }

    public function getControllerName ( array $activeHandler )
    {
        return $this -> getResource ( $activeHandler ) [ 'controllerName' ];
    }

    public function getActionName (array $activeHandler )
    {
        return $this -> getResource ( $activeHandler ) [ 'actionName' ];
    }

    public function getResource ( array $activeHandler )
    {
        # https://forum.phalconphp.com/discussion/17257/detect-controller-instance
        # https://forum.phalconphp.com/discussion/1388/acl-in-micro
        # https://hotexamples.com/examples/phalcon.mvc/Micro/getActiveHandler/php-micro-getactivehandler-method-examples.html
        # $activeHandler[0]=>object(Phalcon\Mvc\Micro\LazyLoader)

        if (
            !isset ( $activeHandler [ 0 ] ) ||
            !$activeHandler [ 0 ] instanceof \Phalcon\Mvc\Micro\LazyLoader
        )
        throw new \InvalidArgumentException ( "Couldn't detect resource controller name." );

        if (
            !isset ( $activeHandler [ 1 ] ) ||
            !is_string ( $activeHandler [ 1 ] ) ||
            empty ( $activeHandler [ 1 ] )
        )
        throw new \InvalidArgumentException ( "Couldn't detect resource action name." );

        $controllerName = $activeHandler [ 0 ] -> getDefinition ();
        $actionName = $activeHandler [ 1 ];

        return [
            'controllerName' => $controllerName,
            'actionName' => $actionName
        ];
    }

}