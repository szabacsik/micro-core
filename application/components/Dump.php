<?php
declare ( strict_types = 1 );

namespace Application\Components;

use Phalcon\Mvc\User\Plugin;

class Dump extends Plugin
{
    public $correlationId = null;
    public $service = null;
    public $request = null;
    public $response = null;
    public $server = null;
    public $additional = null;

    public function __construct ()
    {
    }

    public function add ( $value )
    {
        $this -> additional [] = $value;
    }

    public function push ( $value )
    {
        $this -> add ( $value );
    }

    public function collect ()
    {
        $this -> correlationId = $this -> di -> getShared ( 'correlationHandler' ) -> getCorrelationId ();
        $this -> service = $this -> di -> getShared ( 'configuration' ) -> service;
        $this -> request =
        [
            'host'    => $this -> di -> getShared ( 'request' ) -> getServer ( 'HTTP_HOST' ),
            'uri'     => $this -> di -> getShared ( 'router' ) -> getRewriteUri (),
            'method'  => $this -> di -> getShared ( 'request' ) -> getMethod (),
            'headers' => $this -> di -> getShared ( 'request' ) -> getHeaders (),
            'rawBody' => $this -> di -> getShared ( 'request' ) -> getRawBody ()
        ];
        $this -> response =
        [
            'headers' => (array) $this -> di -> getShared ( 'response' ) -> getHeaders (),
            'body' => $this -> di -> getShared ( 'response' ) -> getContent (),
            'statusCode' => $this -> di -> getShared ( 'response' ) -> getStatusCode (),
            'reasonPhrase' => $this -> di -> getShared ( 'response' ) -> getReasonPhrase ()
        ];
        $this -> server = $_SERVER;
    }

    public function getAll ()
    {
        $this -> collect ();
        return (array) $this;
    }
}
