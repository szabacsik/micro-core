<?php
declare ( strict_types = 1 );
namespace Application\Components;

use Application\Components\Status\httpStatus400;
use Application\Components\Status\restStatusError;
use Application\Exceptions\httpException;

class mergeExceptions
{

    public function __invoke ( array $exceptions )
    {
        return $this -> merge ( $exceptions );
    }

    public static function merge ( array $exceptions )
    {
        self::validate ( $exceptions );

        $exception = null;

        if ( !empty ( $exceptions ) )
        {
            if ( count ( $exceptions ) == 1 )
                $exception = $exceptions [ 0 ];
            else
            {
                //400 Multiple errors encountered
                $restResponse = new restResponse ( new httpStatus400(), new restStatusError(), null );
                foreach ( $exceptions as $exception )
                    foreach ( $exception -> restResponse -> errors as $err )
                        $restResponse -> addMessage ( $err );
                $exception = new httpException ( 'Multiple client errors encountered.', $restResponse, 1004 );
            }
        }

        return $exception;

    }

    private static function validate ( array $exceptions )
    {
        foreach ( $exceptions as $exception )
        {
            $reflect = new \ReflectionClass ( $exception );
            $class = $reflect -> getShortName ();
            //$parentClass = $reflect -> getParentClass () -> getShortName ();
            if ( $class !== 'httpException' )
                throw new \InvalidArgumentException ( 'Invalid exception class. Must be instance of httpException.' );
        }

    }

}
