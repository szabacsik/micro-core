<?php
declare ( strict_types = 1 );
namespace Application\Components;

class upstreamResponseForward
{
    use payloadTrait;

    public $httpStatus = null;
    public $status = null;
    public $messages = null;
    public $errors = null;

    public function __construct ( object $upstreamResponseJsonBody )
    {
        $this -> httpStatus = $upstreamResponseJsonBody -> httpStatus;
        $this -> status = $upstreamResponseJsonBody -> status;
        if ( isset ( $upstreamResponseJsonBody -> messages ) )
            $this -> messages = $upstreamResponseJsonBody -> messages;
        if ( isset ( $upstreamResponseJsonBody -> errors ) )
            $this -> errors = $upstreamResponseJsonBody -> errors;
        if ( isset ( $upstreamResponseJsonBody -> payload ) )
            $this -> payload = $upstreamResponseJsonBody -> payload;
        if ( is_null ( $this -> messages ) )
            unset ( $this -> messages );
        if ( is_null ( $this -> errors ) )
            unset ( $this -> errors );
        if ( is_null ( $this -> payload ) )
            unset ( $this -> payload );
    }
}
