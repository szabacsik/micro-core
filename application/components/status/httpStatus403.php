<?php
declare ( strict_types = 1 );
namespace Application\Components\Status;

class httpStatus403 extends httpStatus
{
    public $code = 403;
    public $definition = 'Forbidden';
    public $description = 'The server understood the request but refuses to authorize it. The client is not permitted access to the resource despite providing authentication such as insufficient permissions of the authenticated account.';
}
