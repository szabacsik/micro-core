<?php
declare ( strict_types = 1 );
namespace Application\Components\Status;

define ( 'VALID_REST_STATUS_CODES', serialize ( [ 'ok', 'info', 'warning', 'error' ] ) );

abstract class restStatus
{
    public $code = null;
    public function __construct ()
    {
        $restStatusCodes = unserialize (VALID_REST_STATUS_CODES );
        if ( !is_string ( $this -> code ) || !in_array ( $this -> code, $restStatusCodes ) )
            throw new \InvalidArgumentException ( "Invalid rest status." );
    }
}
