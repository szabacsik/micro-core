<?php
declare ( strict_types = 1 );
namespace Application\Components\Status;

class httpStatus500 extends httpStatus
{
    public $code = 500;
    public $definition = 'Internal Server Error';
    public $description = 'The server encountered an unexpected condition that prevented it from fulfilling the request.';
}
