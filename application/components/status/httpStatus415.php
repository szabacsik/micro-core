<?php
declare ( strict_types = 1 );
namespace Application\Components\Status;

class httpStatus415 extends httpStatus
{
    public $code = 415;
    public $definition = 'Unsupported Media Type';
    public $description = 'The origin server is refusing to service the request because the payload is in a format not supported by this method on the target resource.';
}
