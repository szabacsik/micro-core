<?php
declare ( strict_types = 1 );
namespace Application\Components\Status;

class httpStatus400 extends httpStatus
{
    public $code = 400;
    public $definition = 'Bad Request';
    public $description = 'The server cannot or will not process the request due to something that is perceived to be a client error (e.g., malformed request syntax, invalid request message framing, or deceptive request routing).';
}
