<?php
declare ( strict_types = 1 );
namespace Application\Components\Status;

//https://httpstatuses.com/
define ( 'VALID_HTTP_STATUS_CODES', serialize ( [ 200, 201, 202, 400, 401, 403, 404, 409, 415, 422, 500, 503 ] ) );

abstract class httpStatus
{
    protected $code = null;
    protected $definition = null;
    protected $description = null;
    public function __construct ()
    {
        $httpStatusCodes = unserialize (VALID_HTTP_STATUS_CODES );
        if ( !is_numeric ( $this -> code ) || !in_array ( $this -> code, $httpStatusCodes ) )
            throw new \InvalidArgumentException ( "Invalid http status code." );
        if ( !is_string ( $this -> definition ) )
            throw new \InvalidArgumentException ( "Invalid http status definition." );
    }
}
