<?php
declare ( strict_types = 1 );
namespace Application\Components\Status;

class restStatusError extends restStatus
{
    public $code = 'error';
}