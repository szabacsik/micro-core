<?php
declare ( strict_types = 1 );
namespace Application\Components\Status;

class httpStatus200 extends httpStatus
{
    public $code = 200;
    public $definition = 'OK';
    public $description = 'The request has succeeded.';
}
