<?php
declare ( strict_types = 1 );
namespace Application\Components\Status;

class httpStatus422 extends httpStatus
{
    public $code = 422;
    public $definition = 'Unprocessable Entity';
    public $description = 'The server understands the content type of the request entity (hence a 415 Unsupported Media Type status code is inappropriate), and the syntax of the request entity is correct (thus a 400 Bad Request status code is inappropriate) but was unable to process the contained instructions.';
}
