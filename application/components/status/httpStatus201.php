<?php
declare ( strict_types = 1 );
namespace Application\Components\Status;

class httpStatus201 extends httpStatus
{
    public $code = 201;
    public $definition = 'Created';
    public $description = 'The request has been fulfilled and has resulted in one or more new resources being created.';
}
