<?php
declare ( strict_types = 1 );
namespace Application\Components\Status;

class httpStatus404 extends httpStatus
{
    public $code = 404;
    public $definition = 'Not Found';
    public $description = 'The origin server did not find a current representation for the target resource or is not willing to disclose that one exists.';
}
