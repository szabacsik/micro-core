<?php
declare ( strict_types = 1 );
namespace Application\Components\Status;

class restStatusOk extends restStatus
{
    public $code = 'ok';
}
