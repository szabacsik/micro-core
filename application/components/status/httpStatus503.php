<?php
declare ( strict_types = 1 );
namespace Application\Components\Status;

class httpStatus503 extends httpStatus
{
    public $code = 503;
    public $definition = 'Service Unavailable';
    public $description = 'The server is currently unable to handle the request due to a temporary overload or scheduled maintenance, which will likely be alleviated after some delay.';
}
