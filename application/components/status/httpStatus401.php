<?php
declare ( strict_types = 1 );
namespace Application\Components\Status;

class httpStatus401 extends httpStatus
{
    public $code = 401;
    public $definition = 'Unauthorized';
    public $description = 'The request has not been applied because it lacks valid authentication credentials for the target resource.';
}
