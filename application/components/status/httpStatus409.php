<?php
declare ( strict_types = 1 );
namespace Application\Components\Status;

class httpStatus409 extends httpStatus
{
    public $code = 409;
    public $definition = 'Conflict';
    public $description = 'The request could not be completed due to a conflict with the current state of the target resource. This code is used in situations where the user might be able to resolve the conflict and resubmit the request.';
}
