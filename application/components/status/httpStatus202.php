<?php
declare ( strict_types = 1 );
namespace Application\Components\Status;

class httpStatus202 extends httpStatus
{
    public $code = 202;
    public $definition = 'Accepted';
    public $description = 'The request has been accepted for processing, but the processing has not been completed. The request might or might not eventually be acted upon, as it might be disallowed when processing actually takes place.';
}
