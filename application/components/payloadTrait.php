<?php
declare ( strict_types = 1 );
namespace Application\Components;

trait payloadTrait
{
    public $payload = null;
    final public function addPayload ( $payload )
    {
        if ( is_null ( $payload ) )
            return;
        if ( is_null ( $this -> payload ) )
            $this -> payload = [];
        $this -> payload [] = $payload;
    }
}
