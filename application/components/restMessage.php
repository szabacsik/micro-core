<?php
declare ( strict_types = 1 );
namespace Application\Components;

class restMessage
{
    use payloadTrait;
    public $code = '';
    public $message = '';
    public $identifier = '';
    public $hint = null;
    public $relatedTo = null;

    const MESSAGE_DATA_JSON_FILENAME = 'restMessages.json';

    public function __construct ( string $message, string $code = '', $payload = null )
    {

        if ( empty ( $message ) && empty ( $code ) )
            throw new \InvalidArgumentException ();

        if ( !empty ( $message ) && !empty ( $code ) )
        {
            $this -> message = $message;
            $this -> code = $code;
        }
        else
        {
            if ( empty ( $code ) )
            {
                $this -> message = $message;
                $this -> lookingForCode ();
            }
            if ( empty ( $message ) )
            {
                $this -> code = $code;
                $this -> lookingForMessage ();
            }
        }

        $this -> setIdentifier ();
        $this -> addPayload ( $payload );
        if ( is_null ( $this -> payload ) )
            unset ( $this -> payload );
        if ( is_null ( $this -> relatedTo ) )
            unset ( $this -> relatedTo );
        if ( is_null ( $this -> hint ) )
            unset ( $this -> hint );
    }

    public function setIdentifier ()
    {
        $this -> identifier = uniqid ('', true ) . '.' . bin2hex ( random_bytes (12 ) );
    }

    private function lookingForCode ()
    {
        $data = $this -> searchBy ( 'message', $this -> message );
        $this -> code = $data [ 'code' ];
    }

    private function lookingForMessage ()
    {
        $data = $this -> searchBy ( 'code', $this -> code );
        $this -> message = $data [ 'text' ];
    }

    private function searchBy ( $field, $value )
    {
        if ( $field === 'message' || $field === 'text' )
            $column = 'text';
        elseif ( $field === 'code' )
            $column = 'code';
        else
            throw new \InvalidArgumentException ();
        $file = dirname (__FILE__ ) . DIRECTORY_SEPARATOR . self::MESSAGE_DATA_JSON_FILENAME;
        $forceAssociativeArray = true;
        $data = json_decode ( file_get_contents ( $file ), $forceAssociativeArray );
        $index = array_search ( $value, array_column ( $data, $column ) );
        if ( is_bool ( $index ) && false === $index )
            throw new \InvalidArgumentException ( "Message field '$field' with value '$value' doesn't exist in '$file'." );
        return $data [ $index ];
    }

}
