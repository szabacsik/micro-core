<?php
declare ( strict_types = 1 );

namespace Application\Components;

use Phalcon\Mvc\User\Plugin;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Psr7;

class Upstream extends Plugin
{
    private $client;
    private $headers;
    protected $response;

    public function __construct ( string $baseUri = '' )
    {
        $correlation = $this -> di -> getShared ( 'correlationHandler' );
        $this -> client = new Client
        ([
            'base_uri' => $baseUri,
        ]);
        $this -> headers =
        [
            'accept' => 'application/json',
            'content-type' => 'application/json',
            $correlation::CORRELATION_HTTP_HEADER_FIELD_NAME => $correlation -> getCorrelationId ()
        ];
        if ( $this -> di -> getShared ( 'request' ) -> hasHeader ( 'Authorization' ) )
            $this -> headers [ 'Authorization' ] = $this -> di -> getShared ( 'request' ) -> getHeader ( 'Authorization' );
    }

    public function request ( string $method, string $uri, $jsonBody )
    {
        try
        {
            $response = $this -> client -> request ( $method, $uri, [ 'headers' => $this -> headers, 'json' => $jsonBody, 'http_errors' => false ] );
        }
        finally
        {}
        return
        [
            'reasonPhrase' => $response -> getReasonPhrase (),
            'statusCode' =>  $response -> getStatusCode (),
            'headers' => $response -> getHeaders (),
            'rawBody' => $response -> getBody () -> getContents (),
            'jsonBody' => json_decode ( (string) $response -> getBody () )
        ];
    }

}
