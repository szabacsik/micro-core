<?php

use Application\Components\restMessage;
use Application\Components\Status as Status;
use Application\Components\mergeExceptions;
use Application\Components\restResponse as restResponse;
use Application\Exceptions\httpException as httpException;
use Phalcon\Mvc\Micro;

require BASE_PATH . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php';
$customConfig = json_decode ( file_get_contents ( BASE_PATH . DIRECTORY_SEPARATOR . 'config.json' ) );
#https://docs.sentry.io/error-reporting/quickstart/?platform=php
Sentry\init ( [ 'dsn' => $customConfig -> SENTRY_DSN ] );
Sentry\configureScope ( function ( Sentry\State\Scope $scope ) use ( $customConfig ): void {
    $scope -> setTag ( 'service', $customConfig -> service );
});
#throw new Exception("My first Sentry error!");

try
{
    $di = require APPLICATION_PATH . DIRECTORY_SEPARATOR . 'configuration' . DIRECTORY_SEPARATOR . 'di.php';
    require APPLICATION_PATH . DIRECTORY_SEPARATOR . 'configuration' . DIRECTORY_SEPARATOR . 'loader.php';
    $app = new \Phalcon\Mvc\Micro ();
    $app -> setDI ( $di );

    $auth = $app -> di -> getShared ( 'auth' );

    $correlationHandler = $app -> di -> getShared ( 'correlationHandler' );
    if ( !$correlationHandler -> validCorrelationIdFormat ( $correlationHandler -> getCorrelationId () ) )
        throw new RuntimeException ( 'The correlation id is missing or invalid.' );

    $collections = require APPLICATION_PATH . DIRECTORY_SEPARATOR . 'configuration' . DIRECTORY_SEPARATOR . 'collections.php';
    foreach ( $collections as $collection )
        $app -> mount ( $collection );

    $app -> before
    (
        function () use ( $app )
        {
            $app -> di -> getShared ( 'acl' ) -> handle ( $app -> getActiveHandler () );
            $exceptions = [];
            $dump = $app -> di -> getShared ( 'dump' );
            $dump -> push ( [ 'before_time' => microtime (true ) ] );
            $app -> di -> get ( 'log' ) -> info ( 'Service started', $dump -> getAll () );
            $request = $app -> di -> get ( 'request' );
            if ( !isset ( $request -> getHeaders () [ 'Content-Type' ] ) || $request -> getHeader ( 'Content-Type' ) != 'application/json' )
            {
                $restResponse = new restResponse ( new Status\httpStatus415(), new Status\restStatusError(), new restMessage ( 'Content-Type must be application/json.' ) );
                $exceptions [] = new httpException ( 'Content-Type must be application/json.', $restResponse, 1002 );
            }

            if ( false === $request -> getJsonRawBody () )
            {
                $restResponse = new restResponse ( new Status\httpStatus400(), new Status\restStatusError(), new restMessage ( 'Malformed JSON, please review the syntax and properties types.' ) );
                $exceptions [] = new httpException ( 'The request could not be understood by the server due to malformed JSON syntax.', $restResponse, 1003 );
            };

            $exception = mergeExceptions::merge ( $exceptions );
            if ( !is_null ( $exception ) )
                throw $exception;

            return true;
        }
    );

    $app -> notFound
    (
        function () use ( $app )
        {
            $app -> di -> getShared ( 'dump' ) -> push ( [ 'notFound_time' => microtime (true ) ] );
            $restResponse = new restResponse ( new Status\httpStatus404(), new Status\restStatusError(), new restMessage ( 'The requested resource could not be found.' ) );
            throw new httpException ( 'Route not defined or the requested resource could not be found.', $restResponse, 1001 );
        }
    );

    $app -> after
    (
        function () use ( $app )
        {
            $payload = null;
            $restResponse = null;

            $return = $app -> getReturnedValue ();

            if ( is_object ( $return ) )
            {
                $reflect = new \ReflectionClass ( $return );
                $class = $reflect -> getShortName ();
                if ( $class === 'restResponse' || $class === 'upstreamResponseForward' )
                    $restResponse = $return;
                else
                    $payload = $return;
            }
            else
                $payload = $return;

            if ( is_null ( $restResponse ) )
                $restResponse = new restResponse ( new Status\httpStatus200(), new Status\restStatusOk(), new restMessage ( 'Ok' ), $payload );

            $response = $app -> response;
            $response -> setStatusCode ( $restResponse -> httpStatus -> code, $restResponse -> httpStatus -> definition );
            $response -> sendHeaders ();
            $response -> setJsonContent ( $restResponse );
            $response -> send ();
            if ( !$app -> response -> isSent () ) {} //dunno
            $dump = $app -> di -> getShared ( 'dump' );
            $dump -> push ( [ 'after_time' => microtime (true ) ] );
            $app -> di -> get ( 'log' ) -> info ( 'Service finished', $dump -> getAll () );
            exit;
        }
    );

    $app -> handle ( str_replace ( '//', '/', $_SERVER [ 'REQUEST_URI' ] ) );
}
catch ( Exception $e )
{
    $reflect = new \ReflectionClass ( $e );
    if ( $reflect -> getShortName () === 'PDOException' )
        $app -> di -> get ( 'db' ) -> rollback ();
    if ( $reflect -> getShortName () === 'httpException' )
        $restResponse = $e -> restResponse;
    else
        $restResponse = new restResponse ( new Status\httpStatus500(), new Status\restStatusError(), new restMessage ( 'We are sorry, something went wrong on our side.' ) );
    $response = $app -> response;
    $response -> setStatusCode ( $restResponse -> httpStatus -> code, $restResponse -> httpStatus -> definition );
    $response -> sendHeaders ();
    $response -> setJsonContent ( $restResponse );
    $response -> send ();
    $dump = $app -> di -> getShared ( 'dump' );
    Sentry\configureScope ( function ( Sentry\State\Scope $scope ) use ( $dump ): void {
        //$scope -> setUser ( [ 'email' => 'a@b.c' ] );
        $scope -> setLevel ( Sentry\Severity::error () );
        $scope -> setExtra ( 'dump', $dump -> getAll () );
    } );
    Sentry\captureException ( $e );
    $di -> get ( 'log' ) -> error ( $e, $dump -> getAll () );
}
finally
{
}
