<?php
declare ( strict_types = 1 );
namespace Application\Exceptions;

class httpException extends \RuntimeException
{
    public $restResponse = null;
    public function __construct ( string $exceptionErrorMessage, $restResponse, int $exceptionErrorCode = 0, \Exception $previous = null )
    {
        $this -> restResponse = $restResponse;
        parent::__construct ( $exceptionErrorMessage, $exceptionErrorCode, $previous );
    }
}