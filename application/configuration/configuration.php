<?php
/**
 * Created by PhpStorm.
 * User: szabacsik
 * Date: 08/03/2018
 * Time: 15:06
 */

use Phalcon\Config;

$config =
[

    'database' =>
    [
        'adapter'  => 'Mysql',
        'host'     => getenv ( 'DB_HOST' ),
        'username' => getenv ( 'DB_USER' ),
        'password' => getenv ( 'DB_PASS' ),
        'name'     => getenv ( 'DB_NAME' ),
        'port'     => 3306
    ],

    'application' =>
    [
        'controllersDirectory' => APPLICATION_PATH . DIRECTORY_SEPARATOR . 'controllers' . DIRECTORY_SEPARATOR,
        'modelsDirectory'      => APPLICATION_PATH . DIRECTORY_SEPARATOR . 'models' . DIRECTORY_SEPARATOR,
        'libraryDirectory'     => APPLICATION_PATH . DIRECTORY_SEPARATOR . 'library' . DIRECTORY_SEPARATOR,
        'exceptionsDirectory'  => APPLICATION_PATH . DIRECTORY_SEPARATOR . 'exceptions' . DIRECTORY_SEPARATOR,
        'componentsDirectory'  => APPLICATION_PATH . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR,
        'statusDirectory'  => APPLICATION_PATH . DIRECTORY_SEPARATOR . 'components' . DIRECTORY_SEPARATOR . 'status',
    ],

];

$customConfigFile = BASE_PATH . DIRECTORY_SEPARATOR . 'config.json';
if ( file_exists ( $customConfigFile ) )
    $customPhalconConfig = new Phalcon\Config\Adapter\Json ( $customConfigFile );

$phalconConfig = new Config ( $config );
$phalconConfig -> merge ( $customPhalconConfig );