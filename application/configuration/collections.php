<?php

//https://docs.phalconphp.com/en/3.4/application-micro#routing-collections

use Phalcon\Mvc\Micro\Collection as MicroCollection;

$test = new MicroCollection ();
$test -> setHandler ( 'Application\Controllers\TestController', true );
$test -> setPrefix ( '/test' );
$test -> map ( '/', 'index' );
$test -> map ( '/sensitive', 'sensitive' );
$test -> map ( '/confidential', 'confidential' );
$test -> map ( '/upstream', 'upstream' );
$test -> map ( '/forward', 'forward' );

$collections = array
(
    "test" => &$test,
);

return $collections;
