<?php

use Phalcon\Di\FactoryDefault;
use Phalcon\Config;
use \Phalcon\Http\Response;
use Phalcon\Db\Adapter\Pdo\Mysql;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;
use Monolog\Handler\LogglyHandler;
use Monolog\Formatter\LogglyFormatter;

$di = new FactoryDefault ();

$di -> setShared
(
    'dump',
    'Application\Components\Dump'
);


$di -> setShared
(
    "configuration",
    function ()
    {
        require 'configuration.php';
        return $phalconConfig;
    }
);

$di -> setShared
(
    "db",
    function ()
    {
        $configuration = $this -> get ( "configuration" );
        return new Mysql
        (
            [
                "adapter"  => $configuration -> database -> adapter,
                "host"     => $configuration -> database -> host,
                "username" => $configuration -> database -> username,
                "password" => $configuration -> database -> password,
                "dbname"   => $configuration -> database -> name,
            ]
        );
    }
);

$di -> setShared
(
    'response',
    function ()
    {
        $response = new Response ();
        $response -> setContentType ( 'application/json', 'utf-8' );
        $correlationHandler = $this -> getShared ( 'correlationHandler' );
        $response -> setHeader ( $correlationHandler::CORRELATION_HTTP_HEADER_FIELD_NAME, $correlationHandler -> getCorrelationId () );
        return $response;
    }
);

$di -> setShared
(
    'log',
    function ()
    {
        $logglyLevel = Logger::toMonologLevel ( $this -> get ( 'configuration' ) -> log -> logglyHandler -> level );
        $logglyCustomerToken = $this -> get ( 'configuration' ) -> log -> logglyHandler -> customerToken;
        $logglyTags = $this -> get ( 'configuration' ) -> log -> logglyHandler -> tags;
        $serviceName = $this -> get ( 'configuration' ) -> service;
        $logglyTags [] = $serviceName;
        $logglyTags = implode ( ',', (array) $logglyTags );
        $streamLevel = Logger::toMonologLevel ( $this -> get ( 'configuration' ) -> log -> streamHandler -> level );
        $streamFilePath = $this -> get ( 'configuration' ) -> log -> streamHandler -> filePath;
        $log = new Logger ( $serviceName );
        $log -> pushHandler ( new StreamHandler ( $streamFilePath, $streamLevel ) );
        $log -> pushHandler ( new LogglyHandler ( "$logglyCustomerToken/tag/$logglyTags/", $logglyLevel ) );
        $log -> pushProcessor ( new \Monolog\Processor\IntrospectionProcessor ( Logger::DEBUG ) );
        return $log;
    }
);

$di -> setShared
(
    "correlationHandler",
    'Application\Components\CorrelationHandler'
);

$di -> set
(
    'upstream',
    'Application\Components\Upstream'
);

$di -> set
(
    'auth',
    'Application\Components\Auth'
);

$di -> set
(
    'acl',
    'Application\Components\Acl'
);

return $di;