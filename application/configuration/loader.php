<?php
use \Phalcon\Loader;

$configuration = $di -> get ( 'configuration' );

$loader = new Loader ();

$loader -> registerFiles ( [ BASE_PATH . DIRECTORY_SEPARATOR . 'vendor' . DIRECTORY_SEPARATOR . 'autoload.php' ] );

$loader -> registerNamespaces
(
    [
        'Application\Controllers' => $configuration -> application -> controllersDirectory,
        'Application\Exceptions' => $configuration -> application -> exceptionsDirectory,
        'Application\Components' => $configuration -> application -> componentsDirectory,
        'Application\Components\Status' => $configuration -> application -> statusDirectory,
        'Application\Models' => $configuration -> application -> modelsDirectory,
    ]
);

$loader -> registerClasses
(
    []
);

$loader -> register ();
