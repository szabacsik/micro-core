<?php
/**
 * Created by PhpStorm.
 * User: szabacsik
 * Date: 08/27/2019
 * Time: 10:42
 */

namespace Application\Controllers;

use Application\Components\restMessage;
use Application\Components\Status\httpStatus200 as httpOk;
use Application\Components\restResponse as restResponse;
use Application\Components\Status\restStatusOk as statusOk;
use Application\Components\upstreamResponseForward;
use Phalcon\Mvc\Controller;

class TestController extends Controller
{
    private $data = null;

    public function onConstruct()
    {
        $object = new \stdClass();
        $object -> property1 = 'value1';
        $object -> property2 = 'value2';
        $object -> property3 = 'value3';
        $this -> data = [ 'array' => [ 'item1', 'item2', 'item3' ], 'object' => $object ];
    }

    public function index ()
    {
        $this -> di -> get ( 'log' ) -> info ( 'The index method called.' );
        $this -> data [ 'upstreamResponse' ] = $this -> di -> get ( 'upstream' ) -> request ( 'get', 'http://localhost/test/upstream', [ 'lorem' => 'ipsum' ] );
        $restResponse = new restResponse ( new httpOk(), new statusOk(), new restMessage ( 'Ok' ), $this -> data );
        $this -> di -> getShared ( 'dump' ) -> push ( [ 'this is' => 'from the index method' ] );
        return $restResponse;
    }

    public function sensitive ()
    {
        return true;
    }

    public function confidential ()
    {
        return true;
    }

    public function forward ()
    {
        $upstreamResponse = $this -> di -> get ( 'upstream' ) -> request ( 'get', 'http://localhost/test', [] );
        return new upstreamResponseForward ( $upstreamResponse [ 'jsonBody' ] );
    }

    public function upstream ()
    {
        $request = new \Phalcon\Http\Request;
        return $request -> getJsonRawBody ();
    }

}
