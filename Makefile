REGISTRY = szabacsik
IMAGE = ubuntu_nginx_php-fpm_phalcon
TAG = latest
NAME = $(REGISTRY)/$(IMAGE):$(TAG)
CONTAINER = micro-core
PWD = $(shell pwd)

run:
	docker run --rm -d -p 80:80 -p 443:443 -p 9001:9001 -v $(PWD):/home/worker/volume/artifact/current --name=$(CONTAINER) $(NAME)

composer-install:
	docker exec -it -u1000 $(CONTAINER) /bin/bash -c 'cd /home/worker/volume/artifact/current && rm -rf composer.lock ./vendor && composer install -v'

curl-test:
	docker exec -it -u1000 $(CONTAINER) /bin/bash -c 'cd /home/worker/volume/artifact/current/tests; chmod +x curl-test.sh; ./curl-test.sh'

codeception-bootstrap:
	docker exec -it -u1000 $(CONTAINER) /bin/bash -c 'cd /home/worker/volume/artifact/current; rm -rf ./_tests'
	docker exec -it -u1000 $(CONTAINER) /bin/bash -c 'mkdir /home/worker/volume/artifact/current/_tests/codeception -p'
	docker exec -it -u1000 $(CONTAINER) /bin/bash -c 'cd /home/worker/volume/artifact/current/_tests/codeception && codecept bootstrap'
	docker exec -it -u1000 $(CONTAINER) /bin/bash -c 'cd /home/worker/volume/artifact/current/_tests/codeception && codecept generate:suite api API_Tester'

codeception-deploy:
	docker exec -it -u1000 $(CONTAINER) /bin/bash -c 'yes | cp -rf /home/worker/volume/artifact/current/tests/* /home/worker/volume/artifact/current/_tests/'

codeception-test:
	docker exec -it -u1000 $(CONTAINER) /bin/bash -c 'cd /home/worker/volume/artifact/current/_tests/codeception && codecept run --steps'

bash:
	docker exec -it -u1000 $(CONTAINER) /bin/bash

stop:
	docker stop $(CONTAINER)

test: curl-test codeception-test

all: run composer-install curl-test codeception-bootstrap codeception-deploy codeception-test stop

nginx-restart:
	docker exec -it -u0 $(CONTAINER) /bin/bash -c 'supervisorctl -u worker -p worker restart nginx'
