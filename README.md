# micro-core
## Phalcon RESTful API Boilerplate
with behavior/scenario-driven object-oriented tests for test driven development approach  

### Create the host directory
```bash
mkdir $PWD/rest
```
This directory will be attached to the container volume located at the `/home/worker/volume` directory within the container.  

### Run container
```bash
docker run --name rest --rm -d -p 80:80 -p 443:443 -p 9001:9001 -v $PWD/rest:/home/worker/volume szabacsik/ubuntu_nginx_php-fpm_phalcon:latest
```
At this point the `phpinfo()` html page should be visible on [http://localhost](http://localhost).  
```bash
wget -qO- http://localhost/ | grep -i 'phalcon' | wc -l
```

### Deploy service
```bash
docker exec -it -u1000 rest bash -c 'cd /home/worker/volume && git clone https://gitlab.com/szabacsik/micro-core.git'
docker exec -it -u1000 rest bash -c 'rm -rf /home/worker/volume/artifact/current/*'
docker exec -it -u1000 rest bash -c 'cp -r /home/worker/volume/micro-core/* /home/worker/volume/artifact/current/'
```

### Install composer dependencies
```bash
docker exec -it -u1000 rest bash -c 'cd /home/worker/volume/artifact/current && composer install'
```

```bash
wget -qO- http://localhost/
```
```json
{"status":"ok","code":200}
```

### Codeception bootstrap
```bash
docker exec -it -u1000 rest bash -c 'mkdir /home/worker/volume/artifact/current/tests/codeception -p'
docker exec -it -u1000 rest bash -c 'cd /home/worker/volume/artifact/current/tests/codeception && codecept bootstrap'
docker exec -it -u1000 rest bash -c 'cd /home/worker/volume/artifact/current/tests/codeception && codecept generate:suite api API_Tester'
docker exec -it -u1000 rest bash -c 'chown worker:worker /home/worker/volume/artifact/current/tests -R'
```

### Deploy tests
```bash
docker exec -it -u1000 rest bash -c 'cp -r /home/worker/volume/micro-core/tests/* /home/worker/volume/artifact/current/tests/'
```

### Execute API tests
```bash
docker exec -it -u1000 rest bash -c 'cd /home/worker/volume/artifact/current/tests/codeception && codecept run --steps'
```

### Create more tests
[Codeception quickstart guide](https://gist.github.com/szabacsik/91c514dde683a5f18f95cea8bceaf74f)

## Codeception

[Bootstrap](https://codeception.com/docs/reference/Commands#Bootstrap "Commands - Codeception - Documentation")

Creates default config, tests directory and sample suites for current project. Use this command to start building a test suite.  

[GenerateSuite](https://codeception.com/docs/reference/Commands#GenerateSuite "Commands - Codeception - Documentation")

Create new test suite.  

[GenerateCest](https://codeception.com/docs/reference/Commands#GenerateCest "Commands - Codeception - Documentation")

Generates Cest ( scenario-driven object-oriented test ) file.  

[Run](https://codeception.com/docs/reference/Commands#Run "Commands - Codeception - Documentation")
