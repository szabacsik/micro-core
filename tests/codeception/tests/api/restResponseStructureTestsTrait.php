<?php

trait restResponseStructureTestsTrait
{

    private function runAllJsonStructureTests ( API_Tester $I )
    {
    }

    private function testHttpResponseHeaders (API_Tester $I )
    {
        $I -> seeHttpHeader ( 'content-type', 'application/json; charset=UTF-8' );
        $I -> seeHttpHeaderOnce ( 'X-Correlation-Id' );
    }


    private function testJson ( API_Tester $I )
    {
        $I -> seeResponseIsJson ();
    }

    private function testJsonStatus ( API_Tester $I )
    {
        $I -> seeResponseJsonMatchesJsonPath ( '$.status' );
        $I -> seeResponseMatchesJsonType ( [ 'status' => 'string:regex(/^(ok|error)$/)' ], '$.' );
    }

    private function testJsonHttpStatus ( API_Tester $I )
    {
        $I -> seeResponseJsonMatchesJsonPath ( '$.httpStatus' );
        $I -> seeResponseJsonMatchesJsonPath ( '$.httpStatus.code' );
        $I -> seeResponseJsonMatchesJsonPath ( '$.httpStatus.definition' );
        $I -> seeResponseJsonMatchesJsonPath ( '$.httpStatus.description' );
        $I -> seeResponseMatchesJsonType (
            [
                'code'        => 'integer',
                'definition'  => 'string',
                'description' => 'string'
            ], '$.httpStatus'
        );
    }

    private function testJsonMessagesOrErrors ( API_Tester $I )
    {
        $status = $I -> grabDataFromResponseByJsonPath ( '$.status' ) [ 0 ];
        switch ( $status )
        {
            case 'ok':
                $I -> seeResponseJsonMatchesJsonPath ( '$.messages' );
                $I -> seeResponseMatchesJsonType ( [ 'messages' => 'array' ] );
                $I -> seeResponseJsonMatchesJsonPath ( '$.messages[*].code' );
                $I -> seeResponseJsonMatchesJsonPath ( '$.messages[*].message' );
                $I -> seeResponseJsonMatchesJsonPath ( '$.messages[*].identifier' );
                $I -> seeResponseMatchesJsonType (
                    [
                        'message' => 'string',
                        'code' => 'string',
                        'identifier' => 'string'
                    ], '$.messages[0]'
                );
            break;
            case 'error':
                $I -> seeResponseJsonMatchesJsonPath ( '$.errors' );
                $I -> seeResponseMatchesJsonType ( [ 'errors' => 'array' ] );
                $I -> seeResponseJsonMatchesJsonPath ( '$.errors[*].code' );
                $I -> seeResponseJsonMatchesJsonPath ( '$.errors[*].message' );
                $I -> seeResponseJsonMatchesJsonPath ( '$.errors[*].identifier' );
                $I -> seeResponseMatchesJsonType (
                    [
                        'message' => 'string',
                        'code' => 'string',
                        'identifier' => 'string'
                    ], '$.errors[0]'
                );
            break;
        }
    }

    private function testJsonHasGotPayload ( API_Tester $I )
    {
        $I -> seeResponseJsonMatchesJsonPath ( '$.payload' );
        $I -> seeResponseMatchesJsonType ( [ 'payload' => 'array|object' ] );
    }

    private function testJsonHasNotGotPayload ( API_Tester $I )
    {
        $I -> dontSeeResponseJsonMatchesJsonPath ( '$.payload' );
    }

}