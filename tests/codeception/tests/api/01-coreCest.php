<?php 

require_once __DIR__ . DIRECTORY_SEPARATOR . 'restResponseStructureTestsTrait.php';

class coreCest
{
    use restResponseStructureTestsTrait;

    private $tokens = null;

    public function _before ( API_Tester $I )
    {
        $this -> tokens [ 'validUser' ] = file_get_contents ( __DIR__ . DIRECTORY_SEPARATOR . 'jwt-validUser.token' );
        $this -> tokens [ 'validUserButExpired' ] = file_get_contents ( __DIR__ . DIRECTORY_SEPARATOR . 'jwt-validUserButExpired.token' );
        $this -> tokens [ 'validAdmin' ] = file_get_contents ( __DIR__ . DIRECTORY_SEPARATOR . 'jwt-validAdmin.token' );
    }

    public function _after ( API_Tester $I )
    {
    }

    public function tryingToTestUnauthenticatedAccessOfPublicResource ( API_Tester $I )
    {
        $I -> haveHttpHeader ( 'Content-Type', 'application/json' );
        $I -> haveHttpHeader ( 'X-Correlation-Id', 'aaaaaaaa-bbbb-4aaa-bbbb-aaaaaaaaaaaa' );
        $I -> sendPOST ( '/test', [] );
        $I -> seeResponseCodeIs ( \Codeception\Util\HttpCode::OK );
    }

    public function tryingToTestUnauthenticatedAccessOfUserResource ( API_Tester $I )
    {
        $I -> haveHttpHeader ( 'Content-Type', 'application/json' );
        $I -> haveHttpHeader ( 'X-Correlation-Id', 'aaaaaaaa-bbbb-4aaa-bbbb-aaaaaaaaaaaa' );
        $I -> sendPOST ( '/test/sensitive', [] );
        $I -> seeResponseCodeIs ( \Codeception\Util\HttpCode::FORBIDDEN );
    }

    public function tryingToTestUnauthenticatedAccessOfAdminResource ( API_Tester $I )
    {
        $I -> haveHttpHeader ( 'Content-Type', 'application/json' );
        $I -> haveHttpHeader ( 'X-Correlation-Id', 'aaaaaaaa-bbbb-4aaa-bbbb-aaaaaaaaaaaa' );
        $I -> sendPOST ( '/test/confidential', [] );
        $I -> seeResponseCodeIs ( \Codeception\Util\HttpCode::FORBIDDEN );
    }

    public function tryingToTestAuthenticatedUserRequestsUserResource ( API_Tester $I )
    {
        $I -> haveHttpHeader ( 'Content-Type', 'application/json' );
        $I -> haveHttpHeader ( 'X-Correlation-Id', 'aaaaaaaa-bbbb-4aaa-bbbb-aaaaaaaaaaaa' );
        $I -> amBearerAuthenticated ( $this -> tokens [ 'validUser' ] );
        $I -> sendPOST ( '/test/sensitive', [] );
        $I -> seeResponseCodeIs ( \Codeception\Util\HttpCode::OK );
    }

    public function tryingToTestAuthenticatedUserRequestsAdminResource ( API_Tester $I )
    {
        $I -> haveHttpHeader ( 'Content-Type', 'application/json' );
        $I -> haveHttpHeader ( 'X-Correlation-Id', 'aaaaaaaa-bbbb-4aaa-bbbb-aaaaaaaaaaaa' );
        $I -> amBearerAuthenticated ( $this -> tokens [ 'validUser' ] );
        $I -> sendPOST ( '/test/confidential', [] );
        $I -> seeResponseCodeIs ( \Codeception\Util\HttpCode::FORBIDDEN );
    }

    public function tryingToTestAuthenticatedAdminRequestsAdminResource ( API_Tester $I )
    {
        $I -> haveHttpHeader ( 'Content-Type', 'application/json' );
        $I -> haveHttpHeader ( 'X-Correlation-Id', 'aaaaaaaa-bbbb-4aaa-bbbb-aaaaaaaaaaaa' );
        $I -> amBearerAuthenticated ( $this -> tokens [ 'validAdmin' ] );
        $I -> sendPOST ( '/test/sensitive', [] );
        $I -> seeResponseCodeIs ( \Codeception\Util\HttpCode::OK );
    }

    public function tryingToTestAuthenticatedUserWithExpiredTokenRequestsUserResource ( API_Tester $I )
    {
        $I -> haveHttpHeader ( 'Content-Type', 'application/json' );
        $I -> haveHttpHeader ( 'X-Correlation-Id', 'aaaaaaaa-bbbb-4aaa-bbbb-aaaaaaaaaaaa' );
        $I -> amBearerAuthenticated ( $this -> tokens [ 'validUserButExpired' ] );
        $I -> sendPOST ( '/test/sensitive', [] );
        $I -> seeResponseCodeIs ( \Codeception\Util\HttpCode::UNAUTHORIZED );
        $I -> seeResponseMatchesJsonType (
            [
                'code' => 'string:regex(/^core-error-006/)',
                'message' => 'string:regex(/^Authorization token has expired.$/)',
                'identifier' => 'string'
            ], '$.errors[0]'
        );
    }

    public function tryToTestBaseUrlRequestSucceeds ( API_Tester $I )
    {
        $I -> haveHttpHeader ( 'Content-Type', 'application/json' );
        $I -> haveHttpHeader ( 'X-Correlation-Id', 'aaaaaaaa-bbbb-4aaa-bbbb-aaaaaaaaaaaa' );
        $I -> amBearerAuthenticated ( $this -> tokens [ 'validUser' ] );
	    $I -> sendPOST ( '/test', [] );
	    $this -> testDirectAndForwardedBaseUrl ( $I );
    }

    public function tryToTestUpstreamResponseForwarder ( API_Tester $I )
    {
        $I -> haveHttpHeader ( 'Content-Type', 'application/json' );
        $I -> haveHttpHeader ( 'X-Correlation-Id', 'aaaaaaaa-bbbb-4aaa-bbbb-aaaaaaaaaaaa' );
        $I -> amBearerAuthenticated ( $this -> tokens [ 'validUser' ] );
        $I -> sendPOST ( '/test/forward', [] );
        $this -> testDirectAndForwardedBaseUrl ( $I );
    }

    private function testDirectAndForwardedBaseUrl ( API_Tester $I )
    {
        $I -> seeResponseCodeIs ( \Codeception\Util\HttpCode::OK );
        $this -> testHttpResponseHeaders ( $I );
        $I -> seeHttpHeader ( 'X-Correlation-Id', 'aaaaaaaa-bbbb-4aaa-bbbb-aaaaaaaaaaaa' );
        $this -> testJson ( $I );
        $this -> testJsonStatus ( $I );
        $this -> testJsonHttpStatus ( $I );
        $this -> testJsonMessagesOrErrors ( $I );
        $this -> testJsonHasGotPayload ( $I );
        $I -> seeResponseJsonMatchesJsonPath ( '$.payload[0].upstreamResponse' );
        $I -> seeResponseMatchesJsonType ( [ 'reasonPhrase' => 'string' ], '$.payload[0].upstreamResponse' );
        $I -> seeResponseMatchesJsonType ( [ 'statusCode' => 'string|integer' ], '$.payload[0].upstreamResponse' );
        $I -> seeResponseMatchesJsonType ( [ 'rawBody' => 'string' ], '$.payload[0].upstreamResponse' );
        $I -> seeResponseMatchesJsonType ( [ 'jsonBody' => 'array' ], '$.payload[0].upstreamResponse' );
        $I -> seeResponseMatchesJsonType ( [ 'headers' => 'array' ], '$.payload[0].upstreamResponse' );
        $I -> seeResponseMatchesJsonType ( [ 'X-Correlation-Id' => 'array' ], '$.payload[0].upstreamResponse.headers' );
        $I -> seeResponseMatchesJsonType ( [ '0' => 'string:regex(/^aaaaaaaa-bbbb-4aaa-bbbb-aaaaaaaaaaaa$/)' ], '$.payload[0].upstreamResponse.headers["X-Correlation-Id"]' );
        $I -> seeResponseMatchesJsonType ( [ 'array' => 'array' ], '$.payload[0]' );
        $I -> seeResponseMatchesJsonType ( [ 'object' => 'array' ], '$.payload[0]' );
        $I -> seeResponseJsonMatchesJsonPath ( '$.payload[0].upstreamResponse.jsonBody.payload' );
        $I -> seeResponseMatchesJsonType ( [ 'lorem' => 'string:regex(/^ipsum$/)' ], '$.payload[0].upstreamResponse.jsonBody.payload[0]' );
    }

    public function tryToTestUndefinedRouteRequestFails ( API_Tester $I )
    {
        $I -> haveHttpHeader ( 'Content-Type', 'application/json' );
        $I -> haveHttpHeader ( 'X-Correlation-Id', 'aaaaaaaa-bbbb-4aaa-bbbb-aaaaaaaaaaaa' );
        $I -> sendPOST ( '/UNDEFINED', [] );
        $I -> seeResponseCodeIs ( \Codeception\Util\HttpCode::NOT_FOUND );
        $this -> testHttpResponseHeaders ( $I );
        $this -> testJson ( $I );
        $this -> testJsonStatus ( $I );
        $this -> testJsonHttpStatus ( $I );
        $this -> testJsonHasNotGotPayload ( $I );
    }

    public function tryToTestMissingCorrelation ( API_Tester $I )
    {
        $I -> haveHttpHeader ( 'Content-Type', 'application/json' );
        $I -> sendPOST ( '/test', [] );
        $I -> seeResponseCodeIs ( \Codeception\Util\HttpCode::INTERNAL_SERVER_ERROR );
        $this -> testHttpResponseHeaders ( $I );
        $this -> testJson ( $I );
        $this -> testJsonStatus ( $I );
        $this -> testJsonHttpStatus ( $I );
        $this -> testJsonMessagesOrErrors ( $I );
    }

}
