#!/bin/bash
expected='"httpStatus":{"code":200,"definition":"OK","description":"The request has succeeded."}'
response=$(curl http://localhost --verbose -i -H 'Content-Type: application/json' -d '{}')
if [[ $response == *"$expected"* ]]; then
  echo -e "Response: $response - OK\n"
  exit 0
else
  echo -e "Response: $response - INVALID\n"
  exit 1
fi