<?php

error_reporting (E_ALL );

define ( 'BASE_PATH', dirname (__DIR__ ) );
define ( 'PUBLIC_PATH', __DIR__ );
define ( 'APPLICATION_PATH', BASE_PATH . DIRECTORY_SEPARATOR . 'application' );

require_once ( APPLICATION_PATH . DIRECTORY_SEPARATOR . 'bootstrap.php' );
